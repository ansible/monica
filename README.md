# Monica

This Ansible role has been tested to install Monica v3.7.0 on Ubuntu 20.04
LXC container sitting behind an Apache reverse proxy.

To be as modular as possible, Monica's required dependencies are installed
and configured using other Ansible roles (see `meta/main.yml`).
These roles are configured using variables that should preferably be set
at playbook-level (in `group_vars/` or similar), and are included below
for reference.

```
# MariaDB
install_mariadb_version: "10.4"
mysql_socket: /var/run/mysqld/mysqld.sock

# NodeJS
nodejs_use_nodesource: true
nodejs_use_tarball: false
nodejs_use_deb: false
nodejs_version: "16.x"
nodejs_install_npm_user: root
npm_config_prefix: /usr/local/lib/npm
npm_config_unsafe_perm: "true"
nodejs_npm_library_global: true
nodejs_npm_packages:
  - name: yarn
    version: 1.22.17

# Apache
apache_enable_ssl: false
apache_remove_default_vhost: true
APACHE_FQDN: monica.internal
apache_mods_enabled:
  - headers.load
  - mpm_event.load
  - proxy.load
  - proxy_fcgi.load
  - setenvif.load
apache_mods_disabled:
  - mpm_prefork.load
  - mpm_prefork.conf
  - "php{{ php_version }}.load"

# PHP
php_version: '8.0'
php_install_recommends: false
php_packages_extra:
  - "libpcre3-dev"
  - "php{{ php_version }}-apcu"
  - "php{{ php_version }}-bcmath"
  - "php{{ php_version }}-cli"
  - "php{{ php_version }}-common"
  - "php{{ php_version }}-curl"
  - "php{{ php_version }}-fpm"
  - "php{{ php_version }}-gd"
  - "php{{ php_version }}-gmp"
  - "php{{ php_version }}-intl"
  # json extension is built-in to PHP8, https://php.watch/versions/8.0/ext-json
  # - "php{{ php_version }}-json"
  - "php{{ php_version }}-mbstring"
  - "php{{ php_version }}-mysql"
  - "php{{ php_version }}-opcache"
  - "php{{ php_version }}-xml"
  - "php{{ php_version }}-zip"
```

On the [recommendation of Monica docs](https://github.com/monicahq/monica/blob/main/docs/installation/providers/generic.md#6-optional-setup-the-queues-with-redis-beanstalk-or-amazon-sqs), let's
**not** attempt to setup async queues with Redis.


## Example playbook

```
- name: Monica LXC container
  hosts: monica_server

  tasks:
    # https://codeberg.org/ansible/common
    - name: "setup common"
      import_role:
        name: common
      become: true
      tags: common
    # https://codeberg.org/ansible/common-systools
    - name: "setup: common-systools"
      import_role:
        name: common-systools
      become: true
    # https://codeberg.org/ansible/ssh
    - name: "ssh"
      import_role:
        name: ssh
      become: true
    - name: "Monica"
      import_role:
        name: monica
      become: true
      tags: monica

```


## Reverse proxy Apache vhost

This role is designed to install Monica on a host running Apache,
sitting behind another host acting as reverse proxy with TLS termination.

Here's the vhost configuration used on that proxy:
```
<VirtualHost *:80>
   ServerAdmin admin@example.se
   ServerName monica.example.se

   Redirect permanent / https://monica.example.se/

   ErrorLog ${APACHE_LOG_DIR}/monica.example.se_error.log
   CustomLog ${APACHE_LOG_DIR}/monica.example.se_access.log combined
</VirtualHost>

<VirtualHost *:443>
   ServerAdmin admin@example.se
   ServerName monica.example.se

   ProxyPreserveHost On
   ProxyRequests Off
   ProxyPass / http://<ip-address-monica>:80/
   ProxyPassReverse / http://<ip-address-monica>:80/

   SSLEngine on
   SSLCertificateKeyFile   /etc/letsencrypt/live/monica.example.se/privkey.pem
   SSLCertificateFile      /etc/letsencrypt/live/monica.example.se/cert.pem
   SSLCertificateChainFile /etc/letsencrypt/live/monica.example.se/chain.pem

   ErrorLog ${APACHE_LOG_DIR}/monica.example.se_error.log
   CustomLog ${APACHE_LOG_DIR}/monica.example.se_access.log combined
</VirtualHost>
```


## Dump Monica's MariaDB database

```
mysqldump --single-transaction --skip-lock-tables --default-character-set=utf8mb4
-u <db_user> -p<db_pass> <db_name> > monica.sql
```


## Refs

+ https://github.com/monicahq/monica/blob/main/docs/installation/providers/generic.md
+ https://github.com/monicahq/monica/blob/master/docs/installation/providers/ubuntu.md
+ https://gitlab.com/CloudArtAdmins/install_monica/
+ https://gitlab.com/mosys/install_monica/
